package cl.ubb.Primo;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;

public class DeterminarPrimoTest {

	@Test
	public void IngresaCeroRetornarPrimoFalso() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(0);
		//asert
		assertThat(resultado,is(false));
	}
	@Test
	public void IngresaUnoRetornarPrimoVerdadero() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(1);
		//asert
		assertThat(resultado,is(true));
	}
	@Test
	public void IngresaDosRetornarPrimoVerdadero() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(2);
		//asert
		assertThat(resultado,is(true));
	}
	@Test
	public void IngresaTresRetornarPrimoVerdadero() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(3);
		//asert
		assertThat(resultado,is(true));
	}
	@Test
	public void IngresacuatroRetornarPrimoFalso() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(4);
		//asert
		assertThat(resultado,is(false));
	}
	@Test
	public void IngresacincoRetornarPrimoVerdadero() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(5);
		//asert
		assertThat(resultado,is(true));
	}
	@Test
	public void IngresaSeisRetornarPrimoFalso() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(6);
		//asert
		assertThat(resultado,is(false));
	}
	public void IngresaSieteRetornarPrimoVerdadero() {
		//arrange
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		//act
		resultado=primo.DeterminarPrimo(7);
		//asert
		assertThat(resultado,is(true));
	}



}
